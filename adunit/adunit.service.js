﻿const db = require('_helpers/db');
const Adunits = db.Adunits;


// DO NOT remove below lines that is for redis
// const redisClient = require('redis').createClient;
// const redis = redisClient(6379, 'localhost');

// redis.on("connect", () => {
//     console.log('connected to Redis');
// });


module.exports = {
    getAll,
    createAdunit,
    getAdunitsBUI
};

async function getAll() {

// DO NOT remove below lines that is for redis

    // return new Promise((resolve, reject) => {
    //     redis.get("adunitList",(err, reply) => {
    //         if(err) {
    //             console.log(err);
    //         } else if(reply) {
    //             console.log("adunit list served from redis.")
    //             resolve(reply);
    //         } else {
    //             Adunit.find({}, (err, adunits) => {
    //                 if(err) {
    //                     return reject(err);
    //                 }
    //                 if(adunits.length > 0) {
    //                     // set in redis
    //                     redis.set("adunitList", JSON.stringify(adunits));
    //                 }
    //                 console.log("adunit list served from mongo.")
    //                 resolve(adunits);
    //             });
    //         }
    //     });
    // });



    return await Adunits.find({});
}

async function createAdunit(args) {
    const adunit = new Adunits(args);
    // save adunit
    return await adunit.save();
}


async function getAdunitsBUI(args) {
    return await Adunits.find({interests: args.interests[0]});
}
