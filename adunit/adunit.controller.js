﻿const express = require('express');
const router = express.Router();
const adunitsService = require('./adunit.service');

// routes
router.get('/getAllAdunits', getAll);
router.post('/createAdunit', create);
router.post('/getAdunitsBUI', getAdunitsBUI);

module.exports = router;

function getAll(req, res, next) {
    adunitsService.getAll()
        .then(adunits => {
            if(typeof adunits === "string")
                adunits = JSON.parse(adunits);

            res.json(adunits)
        })
        .catch(err => next(err));
}

function getAdunitsBUI(req, res, next) {
    console.log(req.body);
    adunitsService.getAdunitsBUI(req.body)
        .then(adunits => {
            if(typeof adunits === "string")
                adunits = JSON.parse(adunits);

            res.json(adunits)
        })
        .catch(err => next(err));
}

function create(req, res, next) {
    adunitsService.createAdunit(req.body)
    .then(data => res.json(data))
    .catch(err => next(err));
}