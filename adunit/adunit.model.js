const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    adID: { type: String, default:'xyz-' },
    title: { type: String },
    content: { type: String },
    image: { type: String },
    video: { type: String },
    url: { type: String },
    createdDate: { type: Date, default: Date.now },
    createdby: { type: String },
    interests: { type: [String], default: [] },
    viewcallbackurl: { type: String },
    likecallbackurl: { type: String },
    clickcallbackurl: { type: String },
    usersusingthisadunit: { type: [String], default: [] },
    basepayoutv: { type: Number },
    basepayoutl: { type: Number },
    basepayoutc: { type: Number },
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Adunits', schema);