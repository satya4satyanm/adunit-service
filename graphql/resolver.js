const db = require('./data/db')
const adunitsService = require('../adunit/adunit.service');


const Mutation = {
   createAdunit:(root,args,context,info) => {


         // create mongo db entry for adunits as well
         adunitsService.createAdunit({
            name:args.name,
            description:args.description,
            type:args.type,
            toppings:args.toppings,
            price:args.price
      });


      // This creates notarealdb entry
         return db.adunits.create({
            name:args.name,
            description:args.description,
            type:args.type,
            toppings:args.toppings,
            price:args.price
      })
   }
}
const Query = {
   greeting:() => "hello"
}

module.exports = {Query,Mutation}