const { DataStore } = require('notarealdb');

const store = new DataStore('./graphql/data');

module.exports = {
   adunits:store.collection('adunits')
};